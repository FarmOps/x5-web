'use strict';

/**
 * @ngdoc function
 * @name x5WebappApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the x5WebappApp
 */
var x5WebappApp = angular.module('x5WebappApp');

x5WebappApp
  .controller('AdminCtrl', function ($scope, $rootScope, AUTH_EVENTS, AuthService, $location, $translate, $sessionStorage) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.credentials = {
	    username: '',
	    password: ''
  	};
  	
	$scope.login = function (credentials) {
	    AuthService.login(credentials).then(function (user) {
	      $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
	      $scope.setCurrentUser();
	      $location.path('/admin/home'); 
	    }, function () {
	      $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
	    });
	};

	$scope.logout = function(){
		AuthService.logout();
		$location.path('/admin');
	};

	$scope.setCurrentUser = function () {
    	$scope.currentUser = $sessionStorage.user;
  	};

  	$scope.changeLanguage = function (langKey) {
    	$translate.use(langKey);
  	};

  	if($sessionStorage.user !== undefined ){
  		$scope.user = $sessionStorage.user.userObject;
  	}  	
  	
  });


x5WebappApp.filter('reverse',function(){
	return function(text){
		return text.split('').reverse().join('');
	};
});

x5WebappApp.filter('status', function(){
	return function(statusCode){
		var statusText = 'Active';
		if(statusCode !== 1){
			statusText = 'Inactive';
		}

		return statusText;
	};
});