'use strict';

/**
 * @ngdoc function
 * @name x5WebappApp.controller:AdminProductsCtrl
 * @description
 * # AdminProductsCtrl
 * Controller of the x5WebappApp
 */
var x5WebappApp = angular.module('x5WebappApp');

x5WebappApp
	.factory('Product', function($cachedResource, sessionInjector){
		return $cachedResource('products','http://localhost:8000/api/v1/products/:id', {id: "@id"},
			{
				query:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				},
				show:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				},
				update:{
					method: 'PUT',
					isArray: false,
					interceptor: sessionInjector
				}
			});
	});

x5WebappApp
  .controller('AdminProductsCtrl',
  	function ($scope, $location, Product) {
	    $scope.awesomeThings = [
	      'HTML5 Boilerplate',
	      'AngularJS',
	      'Karma'
	    ];

	    controllerSetup();

	    Product.query(function(data) {
	    	$scope.products = data.data;
	    	$scope.datacontainer.data = [];

	    	$scope.products.forEach(function(product, index){
	    		$scope.datacontainer.data.push({
	    			'Name': product.name,
	    			'Category': product.category.name,
	    			'Description': product.description,
	    			'Tags': '--',
	    			'Status': product.status,
	    			'id':product.id
	    		});
	    	});
	    });

	    $scope.viewDataItem = function(productId){
	    	$location.path('/admin/products/'+productId);
	    };
	    $scope.editDataItem = function(productId){
	    	$location.path('/admin/products/edit/'+productId);
	    };

	    function controllerSetup(){
	    	$scope.pagetitle = 'Products';
	    	$scope.datacontainer = {
	    		labels: ['Name', 'Category','Description','Tags', 'Status'],
	    		data:[]
	    	};
	    }

  });

  x5WebappApp
  .controller('AdminProductsDetailCtrl',
  	function ($scope, $stateParams, $location, Product) {
	    $scope.awesomeThings = [
	      'HTML5 Boilerplate',
	      'AngularJS',
	      'Karma'
	    ];

	    Product.show({id:$stateParams.id},function(data){
	    	$scope.product = data.data;
	    });
  });

  x5WebappApp
  	.controller('AdminProductsEditCtrl',
  		function($scope, $stateParams, $upload, $window, Product, Category, sessionInjector, flash){
  			Category.query(function(data) {
  				$scope.categories = data.data;
  			});

  			Product.show({id:$stateParams.id}, function(data){
  				$scope.product = data.data;
  			});

  			controllerSetup();

  			$scope.onFileSelect = function($files) {
			    //$files: an array of files selected, each file has name, size, and type.
			    for (var i = 0; i < $files.length; i++) {
			      var file = $files[i];
			      $scope.upload = $upload.upload({
			        url: 'http://localhost:8083/api/v1/productimage/upload/'+$scope.product.id, //upload.php script, node.js route, or servlet url
			        interceptor: sessionInjector,
			        headers: {'Access-Control-Allow-Origin': '*'},
			        data: {myObj: $scope.myModelObj},
			        file: file, // or list of files ($files) for html5 only
			      }).progress(function(evt) {
			        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
			      }).success(function(data, status, headers, config) {
			        // file is uploaded successfully
			        console.log(data);
			      });

			    }

			};

			$scope.updateProduct = function (){
				delete $scope.product.category;
				Product.update($scope.product, function(res){
					var error = res.error;
					if(res.error === false){
						flash.success = 'Product updated correctly';
					}
				});
			};

			$scope.goBack = function (){
				$window.history.back();
			}

			function controllerSetup(){
	    		$scope.pagetitle = 'Edit Product';
	    	}
  	});
