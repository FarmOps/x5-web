'use strict';

var x5WebappApp = angular.module('x5WebappApp');

x5WebappApp
	.factory('Subject', function($cachedResource, sessionInjector){
		return $cachedResource('subjects','http://localhost:8000/api/v1/subjects/:id', {},
			{
				query:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				},
				show:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				}
			});
	});

x5WebappApp
  .controller('AdminSubjectsCtrl',
  	function ($scope, $location, Subject) {
	    $scope.awesomeThings = [
	      'HTML5 Boilerplate',
	      'AngularJS',
	      'Karma'
	    ];
	    controllerSetup();

	    Subject.query(function(data) {
	    	$scope.subjects = data.data;

	    	$scope.subjects.forEach(function(subject, index){
	    		$scope.datacontainer.data.push({
	    			'Name': subject.name,
	    			'Description': subject.description,
	    			'Status': subject.status
	    		});
	    	});
	    });

	    $scope.viewSubject = function(subjectId){
	    	$location.path('/admin/subjects/'+productId);
	    };

	    function controllerSetup(){
	    	$scope.pagetitle = 'Subjects';
	    	$scope.datacontainer = {
	    		labels: ['Name', 'Description','Status'],
	    		data:[]
	    	};
	    }
  });