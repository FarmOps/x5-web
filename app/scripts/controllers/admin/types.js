'use strict';

var x5WebappApp = angular.module('x5WebappApp');

x5WebappApp
	.factory('Type', function($cachedResource, sessionInjector){
		return $cachedResource('types','http://localhost:8000/api/v1/types/:id', {},
			{
				query:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				},
				show:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				}
			});
	});

x5WebappApp
  .controller('AdminTypesCtrl',
  	function ($scope, $location, Type) {
	    $scope.awesomeThings = [
	      'HTML5 Boilerplate',
	      'AngularJS',
	      'Karma'
	    ];

	    controllerSetup();

	    Type.query(function(data) {
	    	$scope.types = data.data;

	    	$scope.types.forEach(function(type, index){
	    		$scope.datacontainer.data.push({
	    			'Name': type.name,
	    			'Subject': type.subject_id,
	    			'Description': type.description,
	    			'Status': type.status
	    		});
	    	});
	    });

	    $scope.viewType = function(subjectId){
	    	$location.path('/admin/types/'+productId);
	    };

	    function controllerSetup(){
	    	$scope.pagetitle = 'Types';
	    	$scope.datacontainer = {
	    		labels: ['Name', 'Subject', 'Description', 'Status'],
	    		data:[]
	    	};
	    }
  });