var x5WebappApp = angular.module('x5WebappApp');

x5WebappApp
	.factory('Order', function($cachedResource, sessionInjector){
		return $cachedResource('orders','http://localhost:8000/api/v1/orders/:id', {id: "@id"},
			{
				query:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				},
				show:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				},
				update:{
					method: 'PUT',
					isArray: false,
					interceptor: sessionInjector
				}
			});
	});

x5WebappApp
  .controller('AdminOrdersCtrl',
  	function ($scope, $location, Order) {
	    $scope.awesomeThings = [
	      'HTML5 Boilerplate',
	      'AngularJS',
	      'Karma'
	    ];

	    controllerSetup();

	    Order.query(function(data) {
	    	$scope.orders = data.data;
	    });

	    function controllerSetup(){
	    	$scope.pagetitle = 'Orders';
	    }
  });