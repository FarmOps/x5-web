'use strict';

/**
 * @ngdoc function
 * @name x5WebappApp.controller:CategoriesCtrl
 * @description
 * # CategoriesCtrl
 * Controller of the x5WebappApp
 */
 var x5WebappApp = angular.module('x5WebappApp');

x5WebappApp
  .controller('CategoriesCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });



x5WebappApp
	.factory('Category', function($cachedResource, sessionInjector){
		return $cachedResource('categories','http://localhost:8000/api/v1/categories/:id', {},
			{
				query:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				}
			});
	});
