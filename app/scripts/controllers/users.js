'use strict';

/**
 * @ngdoc function
 * @name x5WebappApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the x5WebappApp
 */

var x5WebappApp = angular.module('x5WebappApp');

x5WebappApp
  .controller('AdminUsersCtrl', function ($scope, User) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    controllerSetup();

    User.query(function(data) {
	    	$scope.users = data.data;
	});


    function controllerSetup(){
	    	$scope.pagetitle = 'Clients';
	}
  });

x5WebappApp
	.factory('User', function($cachedResource, sessionInjector){
		return $cachedResource('users','http://localhost:8000/api/v1/users/:id', {id: "@id"},
			{
				query:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				},
				show:{
					method:'GET',
					isArray: false,
					interceptor: sessionInjector
				},
				update:{
					method: 'PUT',
					isArray: false,
					interceptor: sessionInjector
				}
			});
	});

