'use strict';

var x5WebappApp = angular.module('x5WebappApp');

x5WebappApp.directive('pageheader',function(){
	return {
		restrict: 'E',
		templateUrl:'views/directives/pageheader.html'
	};
});

x5WebappApp.directive('datacontainer', function(){
	return {
		restrict: 'E',
		templateUrl:'views/directives/datacontainer.html'
	};
});

x5WebappApp.directive('panel', function(){
	return {
		restrict: 'E',
		transclude: true,
		scope:{
			title:'@'
		},
		templateUrl:'views/directives/panel.html'
	};
});